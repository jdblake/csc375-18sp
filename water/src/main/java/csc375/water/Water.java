package csc375.water;

import akka.actor.AbstractActor;
import akka.actor.AbstractActor.Receive;
import akka.actor.ActorSystem;
import akka.actor.ActorRef;
import akka.actor.Props;
import java.io.IOException;
import java.util.Scanner;

class BawaFSM extends AbstractActor {
  private AbstractActor.Receive bawa;
  private AbstractActor.Receive ntah;
  private AbstractActor.Receive nlegoh;
  private AbstractActor.Receive bawa_nka;
  private AbstractActor.Receive nloh;
  private double distance = 0;

  public BawaFSM() {

    bawa =
      receiveBuilder()
        .matchEquals("nloh", s -> {
          distance += 19.0;
          getContext().become(nloh);
        })
        .matchEquals("nlegoh", s -> {
          distance += 12.9;
          getContext().become(nlegoh);
        })
        .matchEquals("ntah", s -> {
          distance += 22.8;
          getContext().become(ntah);
        })
        .matchEquals("bawa-nka", s -> {
          distance += 25.5;
          getContext().become(bawa_nka);
        })
        .matchEquals("bawa", s -> {
        })
        .matchEquals("print", s -> {
          System.out.println("Distance travelled: " + distance);
        })
        .matchAny(o -> System.out.println("(ERROR) Illegal message: " + o))
        .build();

    ntah =
      receiveBuilder()
        .matchEquals("nloh", s -> {
          distance += 36.8;
          getContext().become(nloh);
        })
        .matchEquals("nlegoh", s -> {
          distance += 17.9;
          getContext().become(nlegoh);
        })
        .matchEquals("ntah", s -> {
        })
        .matchEquals("bawa-nka", s -> {
          distance += 34.8;
          getContext().become(bawa_nka);
        })
        .matchEquals("bawa", s -> {
          distance += 22.8;
          getContext().become(bawa);
        })
        .matchEquals("print", s -> {
          System.out.println("Distance travelled: " + distance);
        })
        .matchAny(o -> System.out.println("(ERROR) Illegal message: " + o))
        .build();

    nlegoh =
      receiveBuilder()
        .matchEquals("nlegoh", s -> {
        })
        .matchEquals("ntah", s -> {
          distance += 17.9;
          getContext().become(ntah);
        })
        .matchEquals("bawa-nka", s -> {
          distance += 17.8;
          getContext().become(bawa_nka);
        })
        .matchEquals("bawa", s -> {
          distance += 12.9;
          getContext().become(bawa);
        })
        .matchEquals("print", s -> {
          System.out.println("Distance travelled: " + distance);
        })
        .matchAny(o -> System.out.println("(ERROR) Illegal message: " + o))
        .build();

    bawa_nka =
      receiveBuilder()
        .matchEquals("nlegoh", s -> {
          distance += 17.8;
          getContext().become(nlegoh);
        })
        .matchEquals("ntah", s -> {
          distance += 34.8;
          getContext().become(ntah);
        })
        .matchEquals("nloh", s -> {
          distance += 42.6;
          getContext().become(nloh);
        })
        .matchEquals("bawa-nka", s -> {
        })
        .matchEquals("bawa", s -> {
          distance += 25.5;
          getContext().become(bawa);
        })
        .matchEquals("print", s -> {
          System.out.println("Distance travelled: " + distance);
        })
        .matchAny(o -> System.out.println("(ERROR) Illegal message: " + o))
        .build();

    nloh =
      receiveBuilder()
        .matchEquals("ntah", s -> {
          distance += 36.8;
          getContext().become(ntah);
        })
        .matchEquals("bawa-nka", s -> {
          distance += 42.6;
          getContext().become(bawa_nka);        })
          .matchEquals("nloh", s -> {
          })
          .matchEquals("bawa", s -> {
            distance += 19.0;
            getContext().become(bawa);
          })
        .matchEquals("print", s -> {
          System.out.println("Distance travelled: " + distance);
        })
        .matchAny(o -> System.out.println("(ERROR) Illegal message: " + o))
        .build();

  }

  @Override
  public Receive createReceive() {
    return bawa;
  }

}


public class Water {
  public static void main(String[] args) throws Exception {
    ActorSystem system = ActorSystem.create("Water");

    Scanner in = new Scanner(System.in);

    ActorRef water = system.actorOf(Props.create(BawaFSM.class), "bawa_fsm");

    String res = in.nextLine();

    while (!res.equals("quit")) {
      water.tell(res, ActorRef.noSender());
      res = in.nextLine();
    }

    system.terminate();
  }
}
