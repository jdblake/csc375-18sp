package csc375.firststream;

import akka.stream.*;
import akka.stream.javadsl.*;
import akka.Done;
import akka.NotUsed;
import akka.actor.ActorSystem;
import java.util.concurrent.CompletionStage;

public class FirstStream {

  public static void main(String[] args) {
    // Make an ActorSystem so actors can run
    final ActorSystem system = ActorSystem.create("FirstStream");
    // Make an actor factory for the system
    final Materializer materializer = ActorMaterializer.create(system);

    // Make a Source (<return type, additional info if needed>)
    final Source<Integer, NotUsed> source = Source.range(1, 100);

    // Sources tell you if they are done so you can shut down the system
    //    (can also use other methods)
    final CompletionStage<Done> done =
      source.runForeach(i -> System.out.println(i), materializer);
      //source.map(i -> { return(i * i); }).runForeach(i -> System.out.println(i), materializer);

    done.thenRun(() -> system.terminate());
  }
}
