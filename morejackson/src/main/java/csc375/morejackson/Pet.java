package csc375.morejackson;

import java.util.List;

class Pet {

  private int id;
  private String type;
  private String name;
  private int age;
  private List<Integer> friends;
  private String[] favFoods;

  public Pet() {

  }

  public Pet(Integer id, String type, String name, Integer age, List<Integer> friends,
      String[] favFoods) {
    this(id, type, name, age);
    this.friends = friends;
    this.favFoods = favFoods;
  }

  public Pet(Integer id, String type, String name, Integer age) {
    this.id = id;
    this.type = type;
    this.name = name;
    this.age = age;
    this.friends = null;
    this.favFoods = null;
  }

  public Integer getId() {
    return (id);
  }

  public String getType() {
    return (type);
  }

  public String getName() {
    return (name);
  }

  public Integer getAge() {
    return (age);
  }

  public List<Integer> getFriends() {
    return (friends);
  }

  public String[] getFavFoods() {
    return (favFoods);
  }

  public void setId(int id) {
    this.id = id;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setType(String type) {
    this.type = type;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public void setFriends(List<Integer> friends) {
    this.friends = friends;
  }

  public void setFavFoods(String[] favFoods) {
    this.favFoods = favFoods;
  }

  public boolean equals(Pet p) {
    return (toString().equals(p.toString()));
  }

  public String toString() {
    return ("[ID:" + id + ",Type:" + type + ",Name:" + name + ",Age:" + age + ",Friends:" + friends
        + ",Foods:" + ((favFoods == null) ? "null" : "[" + String.join(",", favFoods) + "]") + "]");
  }
}
