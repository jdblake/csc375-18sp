package csc375.morejackson;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.core.type.TypeReference;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;

public class MoreJackson {

  public static void main(String[] args) throws JsonProcessingException, IOException {

    // Make a Pet
    Pet lily = new Pet(1, "Dog", "Lily", 4);

    // Print out toString representation
    System.out.println("Lily toString: " + lily);

    // Get mapper
    ObjectMapper mapper = new ObjectMapper();

    // Pretty-print for debugging (NOT FOR PRODUCTION!)
    mapper.configure(SerializationFeature.INDENT_OUTPUT, true);

    // Marshall lily and print json
    String lilyJson = mapper.writeValueAsString(lily);
    System.out.println("Marshall lily (pre):\n" + lilyJson);

    // Add foods and friends
    String[] foods = {"Pumpkin", "Chicken", "Sweet Potato", "Bacon"};
    lily.setFavFoods(foods);

    List<Integer> friends = new ArrayList<Integer>();
    friends.add(2);
    friends.add(3);
    friends.add(6);

    lily.setFriends(friends);

    // Marshall lily again, and print json
    lilyJson = mapper.writeValueAsString(lily);
    System.out.println("Marshall lily (post):\n" + lilyJson);

    // Automatically create class from JSON
    Pet lilyCopy = mapper.readValue(lilyJson, Pet.class);
    System.out.println("Lily Copy: " + lilyCopy);
    System.out.println("Lily Copy Same: " + lilyCopy.equals(lily));

    // Manually extract items
    // Marshall, then get the friend list and food array
    JsonNode lilyRoot = mapper.readTree(lilyJson);
    JsonNode lilyFriends = lilyRoot.get("friends");
    JsonNode lilyFoods = lilyRoot.get("favFoods");

    // Turn friends to List<Integer> for unmarshalling
    ObjectReader friendReader = mapper.readerFor(new TypeReference<List<Integer>>() {});
    List<Integer> friendList = friendReader.readValue(lilyFriends);

    // Print unmarshalled friendList
    System.out.println("Marshalled Friends: " + friendList);

    // Turn friends to List<Integer> for unmarshalling
    ObjectReader foodReader = mapper.readerFor(new TypeReference<String[]>() {});
    String[] foodArray = foodReader.readValue(lilyFoods);

    // Print unmarshalled favFoods
    System.out.println("Marshalled Favorite Foods: [" + String.join(", ", foodArray) + "]");

  }
}
