#include <iostream>
#include <fstream>
#include <stdlib.h>

using namespace std;

void render(int size, int level, int *pts) {

  for (int i = 0; i < size*size; i++) {
    float xPos = -2.0 + (i % size) * 3.0 / size;
    float yPos = -1.5 + (i / size) * 3.0 / size;

    float x = 0;
    float y = 0;

    int iter = 0;
    do {
      float xTemp = x*x-y*y + xPos;
      y = 2 * x * y + yPos;
      x = xTemp;
      iter++;
    } while (x*x + y*y <= 4 && iter < level);

    pts[i] = (x*x + y*y <= 4)?0:(iter*255)/level;
  }
}

int main(int argc, char* argv[])
{
  if (argc != 4) {
    cout << "SYNTAX: mandelbrot-cpu <size> <level> <file_out>\n";
    exit(-1);
  }
  int size = atoi(argv[1]);
  int level = atoi(argv[2]);
  int tot = size*size;

  int *pts = new int[tot];

  render(size, level, pts);

  ofstream outFile;
  outFile.open(argv[3]);
  outFile << "P2\n" << size << " " << size << "\n255" << endl;

  for (int i = 0; i < tot; i++)
    outFile << " " << pts[i];

  outFile.close();

  // Free memory
  delete(pts);

  return 0;
}
