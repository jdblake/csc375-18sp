#include <iostream>
#include <fstream>

using namespace std;

// Kernel function to add the elements of two arrays
__global__
void render(int n, int size, int level, int *pts) {
  int index = blockIdx.x * blockDim.x + threadIdx.x;
  int stride = blockDim.x * gridDim.x;

  for (int i = index; i < n; i += stride) {
    float xPos = -2.0 + (i % size) * 3.0 / size;
    float yPos = -1.5 + (i / size) * 3.0 / size;

    float x = 0;
    float y = 0;

    int iter = 0;
    do {
      float xTemp = x*x-y*y + xPos;
      y = 2 * x * y + yPos;
      x = xTemp;
      iter++;
    } while (x*x + y*y <= 4 && iter < level);

    pts[i] = (x*x + y*y <= 4)?0:(iter*255)/level;
  }
}

int main(int argc, char* argv[])
{
  if (argc != 4) {
    cout << "SYNTAX: mandelbrot <size> <level> <file_out>\n";
    exit(-1);
  }
  int size = atoi(argv[1]);
  int level = atoi(argv[2]);
  int tot = size*size;
  int *pts;

  // Allocate Unified Memory – accessible from CPU or GPU
  cudaMallocManaged(&pts, tot*sizeof(int));

  int blockSize = 1024;
  int numBlocks = (tot + blockSize - 1) / blockSize;
  render<<<numBlocks, blockSize>>>(tot, size, level, pts);
  //render<<<1,1>>>(tot, size, level, pts);

  // Wait for GPU to finish before accessing on host
  cudaDeviceSynchronize();

  ofstream outFile;
  outFile.open(argv[3]);
  // Dump pgm image
  outFile << "P2\n" << size << " " << size << "\n255" << endl;

  for (int i = 0; i < tot; i++)
    outFile << " " << pts[i];

  outFile.close();

  // Free memory
  cudaFree(pts);

  return 0;
}
