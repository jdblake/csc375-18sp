#include <iostream>

using namespace std;

// function to swap two ints
void swap(int a, int b)
{
  int t = a;
  a = b;
  b = t;
}

int main(void) {
  int a = 3;
  int b = 4;
  cout << "a: " << a << endl;
  cout << "b: " << b << endl;
  cout << "...SWAPPING..." << endl;
  swap(a,b);
  cout << "a: " << a << endl;
  cout << "b: " << b << endl;
}
