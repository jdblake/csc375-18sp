#include <iostream>
#include <stdlib.h>

using namespace std;

int main(void) {

  int *arr = (int *) calloc(10,sizeof(int));

  // initialize x and y arrays on the host
  for (int i = 0; i < 10; i++) {
    arr[i] = i;
  }

  cout << arr << endl;

  int *second_addr = arr + 1;

  int second_val = *second_addr;

  cout << second_addr << "\t" << second_val << endl;

  short *third_addr = (short *) (arr) + (short) 1;

  cout << third_addr << endl;

  int t = 4;

  short *cool = (short *) &t;

  cout << cool[0] << "\t" << cool[1] << endl;

}
