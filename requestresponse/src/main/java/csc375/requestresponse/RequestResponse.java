package csc375.requestresponse;

import akka.http.javadsl.model.*;
import akka.http.javadsl.model.headers.*;
import akka.http.javadsl.model.HttpProtocols.*;
import akka.http.scaladsl.model.MediaTypes.*;
import akka.util.*;
import java.util.Optional;

public class RequestResponse {
  public static void main(String[] args) {
    // construct a simple GET request to `homeUri`
    Uri homeUri = Uri.create("/home");
    HttpRequest request1 = HttpRequest.create().withUri(homeUri);

    // construct simple GET request to "/index" using helper methods
    HttpRequest request2 = HttpRequest.GET("/index");

    // construct simple POST request containing entity
    ByteString data = ByteString.fromString("abc");
    HttpRequest postRequest1 = HttpRequest.POST("/receive").withEntity(data);

    // customize every detail of HTTP request
    //import HttpProtocols.*
    //import MediaTypes.*
    Authorization authorization = Authorization.basic("user", "pass");
    HttpRequest complexRequest =
        HttpRequest.PUT("/user")
            .withEntity(HttpEntities.create(ContentTypes.TEXT_PLAIN_UTF8, "abc"))
            .addHeader(authorization).withProtocol(HttpProtocols.HTTP_1_0);
  }
}
