package csc375.shutdown;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.event.Logging;
import akka.event.LoggingAdapter;

public class ShutdownActor extends AbstractActor {

  public static class Stop {
    public Stop() {}
  }

  public static class Hello {
    public Hello() {}
  }

  private int helloCount;
  private int id;

  private LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);

  public ShutdownActor(int id) {
    helloCount = 0;
    this.id = id;
  }

  public static Props props(int id) {
    return Props.create(ShutdownActor.class, () -> new ShutdownActor(id));
  }

  @Override
  public void preStart() {
    System.out.println("Shutdown " + id + " started.");
  }

  @Override
  public void postStop() {
    System.out.println("Shutdown " + id + " stopped (" + helloCount + ")");
  }

  @Override
  public Receive createReceive() {
    return receiveBuilder()
      .match(Hello.class, s -> {
        helloCount += 1;
        log.info(id + " got Hello: " + helloCount);
      })
      .match(Stop.class, s -> {
        getContext().stop(getSelf());
      })
      .matchAny(o -> log.info("received unknown message"))
      .build();
  }
}
