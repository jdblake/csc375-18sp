package csc375.shutdown;

import akka.actor.PoisonPill;
import akka.actor.ActorSystem;
import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import java.io.IOException;
import csc375.shutdown.ShutdownActor.*;

public class Shutdown {

  public static void main(String[] args) throws IOException {
    ActorSystem system = ActorSystem.create("Shut");

    ActorRef shutdownActor1 = system.actorOf(ShutdownActor.props(1), "shutdownActor1");
    shutdownActor1.tell(new Hello(), ActorRef.noSender());
    shutdownActor1.tell(PoisonPill.getInstance(), ActorRef.noSender());
    shutdownActor1.tell(new Hello(), ActorRef.noSender());

    ActorRef shutdownActor2 = system.actorOf(ShutdownActor.props(2), "shutdownActor2");
    shutdownActor2.tell(new Hello(), ActorRef.noSender());
    shutdownActor2.tell(new Stop(), ActorRef.noSender());
    shutdownActor2.tell(new Hello(), ActorRef.noSender());

    //Thread.sleep(1000);

    System.out.println(">>> Press ENTER to exit <<<");
    try {
      System.in.read();
    } finally {
      system.terminate();
    }

  }
}
