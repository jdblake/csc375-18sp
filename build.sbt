name := "csc375_projects"

version := "0.1"

scalaVersion := "2.12.4"

lazy val become = project
  .settings(
    name := "become",
    libraryDependencies ++= Seq(dependencies.akkaActor)
  )

lazy val device = project
  .settings(
    name := "device",
    libraryDependencies ++= Seq(dependencies.akkaActor)
  )

lazy val failure = project
  .settings(
    name := "failure",
    libraryDependencies ++= Seq(dependencies.akkaActor)
  )

lazy val hierarchy = project
  .settings(
    name := "hierarchy",
    libraryDependencies ++= Seq(dependencies.akkaActor)
)

lazy val lifecycle = project
  .settings(
    name := "lifecycle",
    libraryDependencies ++= Seq(dependencies.akkaActor)
  )

lazy val quickstart = project
  .settings(
    name := "quickstart",
    libraryDependencies ++= Seq(dependencies.akkaActor)
  )

lazy val shutdown = project
  .settings(
    name := "shutdown",
    libraryDependencies ++= Seq(dependencies.akkaActor)
  )

lazy val water = project
  .settings(
    name := "water",
    libraryDependencies ++= Seq(dependencies.akkaActor)
  )

lazy val httpquickstart = project
  .settings(
    name := "httpquickstart",
    libraryDependencies ++= Seq(
      dependencies.akkaStream,
      dependencies.akkaHttp,
      dependencies.akkaJackson)
  )

lazy val httpminimalexample = project
  .settings(
    name := "httpminimalexample",
    libraryDependencies ++= Seq(
      dependencies.akkaStream,
      dependencies.akkaHttp)
  )

lazy val minimalhttpapp = project
  .settings(
    name := "minimalhttpapp",
    libraryDependencies ++= Seq(
      dependencies.akkaStream,
      dependencies.akkaHttp)
  )

lazy val simplehttpserver = project
  .settings(
    name := "simplehttpserver",
    libraryDependencies ++= Seq(
      dependencies.akkaStream,
      dependencies.akkaHttp)
  )

lazy val firststream = project
  .settings(
    name := "firststream",
    libraryDependencies ++= Seq(dependencies.akkaStream)
  )

lazy val secondstream = project
  .settings(
    name := "secondstream",
    libraryDependencies ++= Seq(dependencies.akkaStream)
  )

lazy val simplejackson = project
  .settings(
    name := "simplejackson",
    libraryDependencies ++= Seq(dependencies.jacksonDatabind)
  )

lazy val morejackson = project
  .settings(
    name := "morejackson",
    libraryDependencies ++= Seq(dependencies.jacksonDatabind)
  )

lazy val requestresponse = project
  .settings(
    name := "requestresponse",
    libraryDependencies ++= Seq(
      dependencies.akkaStream,
      dependencies.akkaHttp)
  )

lazy val highlevelserverexample = project
  .settings(
    name := "highlevelserverexample",
    libraryDependencies ++= Seq(
      dependencies.akkaActor,
      dependencies.akkaStream,
      dependencies.akkaHttp)
  )

lazy val jacksonexampletest = project
  .settings(
    name := "jacksonexampletest",
    libraryDependencies ++= Seq(
      dependencies.akkaStream,
      dependencies.akkaJackson,
      dependencies.akkaHttp)
  )

lazy val dependencies =
  new {
    val akkaActorV         = "2.5.11"
    val akkaStreamV        = "2.5.11"
    val akkaHttpV          = "10.1.0"
    val jacksonV           = "2.9.5"

    val akkaActor          = "com.typesafe.akka"   %% "akka-actor"        % akkaActorV
    val akkaStream         = "com.typesafe.akka"   %% "akka-stream"       % akkaStreamV
    val akkaHttp           = "com.typesafe.akka"   %% "akka-http"         % akkaHttpV
    val akkaJackson        = "com.typesafe.akka"   %% "akka-http-jackson" % akkaHttpV
    val jacksonCore        = "com.fasterxml.jackson.core" % "jackson-core" % jacksonV
    val jacksonDatabind    = "com.fasterxml.jackson.core" % "jackson-databind" % jacksonV
    val jacksonAnnotations = "com.fasterxml.jackson.core" % "jackson-annotations" % jacksonV
  }
