package csc375.simplejackson;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import java.io.IOException;

public class SimpleJackson {

  public static void main(String[] args) throws JsonProcessingException, IOException {
    PetPublic p1 = new PetPublic(1, "Dog", "Fido", 3);
    PetPrivate p2 = new PetPrivate(2, "Cat", "Fluffy", 5);

    System.out.println("P1: " + p1);
    System.out.println("P2: " + p2);

    // Get mapper
    ObjectMapper mapper = new ObjectMapper();

    // Pretty-print for debugging (NOT FOR PRODUCTION!)
    mapper.configure(SerializationFeature.INDENT_OUTPUT, true);

    // Marshall pets
    String m1 = mapper.writeValueAsString(p1);
    System.out.println("Marshall 1: " + m1);

    String m2 = mapper.writeValueAsString(p2);
    System.out.println("Marshall 2: " + m2);

    // Automatically create class from JSON
    PetPublic p3 = mapper.readValue(m1, PetPublic.class);
    //System.out.println("Lily Copy: " + lilyCopy);

    // Automatically create class from JSON
    PetPrivate p4 = mapper.readValue(m2, PetPrivate.class);
    //System.out.println("Lily Copy: " + lilyCopy);

    /*
     // Marshall to JsonNodes
     JsonNode m1 = mapper.readTree(u1);
     JsonNode m2 = mapper.readTree(u2);

     // Create new PetPublic
     PetPublic p3 =
     new PetPublic(m1.get("id").intValue(), m1.get("type").textValue(), m1.get("name")
     .textValue(), m1.get("age").intValue());

     // Create new PetPrivate
     PetPrivate p4 =
     new PetPrivate(m2.get("id").intValue(), m2.get("type").textValue(), m2.get("name")
     .textValue(), m2.get("age").intValue());
     */

    // Check that pets match
    System.out.println("Pet Public Match: " + p1.equals(p3));

    // Check that pets match
    System.out.println("Pet Private Match: " + p2.equals(p4));
  }
}
