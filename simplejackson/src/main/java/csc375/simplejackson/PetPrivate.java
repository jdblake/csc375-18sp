package csc375.simplejackson;

class PetPrivate {

  private int id = 0;
  private String type = "";
  private String name = "";
  private int age = 0;

  public PetPrivate() {

  }

  public PetPrivate(int id, String type, String name, int age) {
    this.id = id;
    this.type = type;
    this.name = name;
    this.age = age;
  }

  public int getId() {
    return (id);
  }

  public String getType() {
    return (type);
  }

  public String getName() {
    return (name);
  }

  public int getAge() {
    return (age);
  }

  public void setId(int id) {
    this.id = id;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setType(String type) {
    this.type = type;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public boolean equals(PetPrivate p) {
    return (p.toString().equals(toString()));
  }

  public String toString() {
    return ("[ID:" + id + ",Type:" + type + ",Name:" + name + ",Age:" + age + "]");
  }
}
