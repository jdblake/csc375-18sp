package csc375.simplejackson;

class PetPublic {

  public int id = 0;
  public String type = "";
  public String name = "";
  public int age = 0;

  public PetPublic() {}

  public PetPublic(Integer id, String type, String name, Integer age) {
    this.id = id;
    this.type = type;
    this.name = name;
    this.age = age;
  }

  public void setId(int id) {
    this.id = id;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setType(String type) {
    this.type = type;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public boolean equals(PetPublic p) {
    return (toString().equals(p.toString()));
  }

  public String toString() {
    return ("[ID:" + id + ",Type:" + type + ",Name:" + name + ",Age:" + age + "]");
  }
}
