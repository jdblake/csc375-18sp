package csc375.minimalhttpexample;

import akka.Done;
import akka.actor.ActorSystem;
import akka.http.javadsl.server.HttpApp;
import akka.http.javadsl.server.Route;
import java.util.Optional;
import java.util.concurrent.*;

// Server definition
public class MinimalHttpApp extends HttpApp {

  @Override
  protected Route routes() {
    return route(
      path("hello", () ->
        get(() ->
          complete("<h1>Say hello now to akka-http</h1>")
        )),
      path("hello", () ->
        put(() ->
          complete("<h1>Putting hello at akka-http</h1>")
        )),
      path("goodbye", () ->
          get(() ->
          complete("<h1>Say goodbye now to akka-http</h1>")
        ))
    );
  }

  public static void main(String[] args) throws Exception {
    // Starting the server
    final MinimalHttpApp myServer = new MinimalHttpApp();
    myServer.startServer("localhost", 8080);
  }

}
