package csc375.firststream;

import akka.stream.*;
import akka.stream.javadsl.*;
import akka.Done;
import akka.NotUsed;
import akka.actor.ActorSystem;
import java.util.concurrent.CompletionStage;
import java.util.Arrays;

public class SecondStream {

  public static void main(String[] args) {
    // Make an ActorSystem so actors can run
    final ActorSystem system = ActorSystem.create("FirstStream");
    // Make an actor factory for the system
    final Materializer mat = ActorMaterializer.create(system);

    // Set the source
    final Source<Integer, NotUsed> source =
        Source.from(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
    final Sink<Integer, CompletionStage<Integer>> sink =
        Sink.<Integer, Integer> fold(0, (aggr, next) -> aggr + next);

/* 1 *******
    // materialize the flow, getting the Sinks materialized value
    final CompletionStage<Integer> sum = source.runWith(sink, mat);
************/

/* 2 ********/
    // connect the Source to the Sink, obtaining a RunnableFlow
    final RunnableGraph<CompletionStage<Integer>> runnable =
      source.toMat(sink, Keep.right());
    // materialize the flow
    final CompletionStage<Integer> sum = runnable.run(mat);
/************/

    // Print the result then terminate the ActorSystem
    sum.thenAccept(i -> System.out.println(i)).thenRun(() -> system.terminate());
  }
}
