package csc375.become;

import akka.actor.AbstractActor;
import akka.actor.AbstractActor.Receive;
import akka.actor.ActorSystem;
import akka.actor.ActorRef;
import akka.actor.Props;
import java.io.IOException;

class HotSwapActor extends AbstractActor {
  private AbstractActor.Receive foo;
  private AbstractActor.Receive bar;

  public HotSwapActor() {
    foo =
      receiveBuilder()
        .matchEquals("foo", s -> {
          System.out.println("I am foo already!");
        })
        .matchEquals("bar", s -> {
          System.out.println("Becoming bar...");
          getContext().become(bar);
        })
        .build();

    bar = receiveBuilder()
      .matchEquals("bar", s -> {
          System.out.println("I am bar already!");
      })
      .matchEquals("foo", s -> {
        System.out.println("Becoming foo...");
        getContext().become(foo);
      })
      .build();
  }

  @Override
  public Receive createReceive() {
    return receiveBuilder()
      .matchEquals("foo", s -> {
        System.out.println("Becoming foo...");
        getContext().become(foo);
      })
      .matchEquals("bar", s -> {
        System.out.println("Becoming bar...");
        getContext().become(bar);
      })
      .matchAny(s -> {
        System.out.println("Unknown message: " + s);
      })
      .build();
  }
}


public class BecomeUnbecome {
  public static void main(String[] args) throws IOException {
    ActorSystem system = ActorSystem.create("Become");

    ActorRef hotswap = system.actorOf(Props.create(HotSwapActor.class), "hotswap");
    hotswap.tell("test", ActorRef.noSender());
    hotswap.tell("foo", ActorRef.noSender());
    hotswap.tell("bar", ActorRef.noSender());
    hotswap.tell("bar", ActorRef.noSender());
    hotswap.tell("foo", ActorRef.noSender());
    hotswap.tell("bar", ActorRef.noSender());
    hotswap.tell("foo", ActorRef.noSender());
    hotswap.tell("foo", ActorRef.noSender());

    System.out.println(">>> Press ENTER to exit <<<");
    try {
      System.in.read();
    } finally {
      system.terminate();
    }

  }
}
